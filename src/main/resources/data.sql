INSERT INTO THEATER  VALUES (1, 'Star Cinema Bagatelle - Updated','Bagatelle Mall');
INSERT INTO THEATER  VALUES (2, 'Star Croisette','Grand Baie');
INSERT INTO THEATER  VALUES (3, 'Star Cinema Le Caudan Waterfront','Port-Louis, Caudan Waterfront');
INSERT INTO THEATER  VALUES (4, 'Mcine Trianon','Quatre Bornes, La City Trianon');
INSERT INTO THEATER  VALUES (5, 'Cinema Novelty','Curepipe');
INSERT INTO THEATER  VALUES (6, 'MCine Flacq','Central Flacq');
INSERT INTO THEATER  VALUES (7, 'Cine METROPOLITAN Vieux Moulin','Rose Belle, In Vieux Moulin Shopping Centre');


INSERT INTO MOVIE VALUES(1,  'https://images.app.goo.gl/f9yHJptfSU8TMT7r6','Action', 'Dark Phoenix', 2020);
INSERT INTO MOVIE VALUES(2,  'https://images.app.goo.gl/f9yHJptfSU8TMT7r6','Action', 'Die hard', 2000);

INSERT INTO CATEGORY ("NAME","CODE","DESCRIPTION") VALUES('Appliances', '100', '');
INSERT INTO CATEGORY ("NAME","CODE","DESCRIPTION") VALUES('Electronics', '101', '');
INSERT INTO CATEGORY ("NAME","CODE","DESCRIPTION") VALUES( 'Home and Garden', '102', '');
INSERT INTO CATEGORY ("NAME","CODE","DESCRIPTION") VALUES( 'Furniture', '103', '');
INSERT INTO CATEGORY ("NAME","CODE","DESCRIPTION") VALUES( 'Baby and Kids', '104', '');
INSERT INTO CATEGORY ("NAME","CODE","DESCRIPTION") VALUES( 'Fitness', '105', '');
-- INSERT INTO CATEGORY VALUES('Fitness', 106, '')

INSERT INTO "PRODUCT"("CATEGORY","DESCRIPTION","IMG_URL","NAME","PRICE","STOCK_AVAILABLE")VALUES
('Appliances', 'Power: 1700W Capacity: 5.5L With removable non-stick coated basket is PFOA-free', 'https://priceguru.mu/media/catalog/product/cache/021a5445bd4418c7815af89045564cdd/p/r/priceguru-nutricook-5.5l_1.jpg','Nutricook Air Fryer 2.0 (5.5L)', 7290, 100),
('Appliances', 'Product Type: Vacuum Cleaner Rated voltage: 21.6V Rated power: 150W', 'https://priceguru.mu/media/catalog/product/cache/c5b0e6136a6dd7f7d91d8b889ed40f35/p/r/priceguru-vc25_1.jpg','Xiaomi Deerma Handheld Vacuum Cleaner VC25', 3990, 99),
('Appliances', 'Type: Citrus Juicer
Power: 60W
2 Juicing cones for different fruit sizes', 'https://priceguru.mu/media/catalog/product/cache/c5b0e6136a6dd7f7d91d8b889ed40f35/p/r/priceguru-pacific-kp-602a.jpg','Pacific Citrus Juicer 60W', 635, 15),
('Electronics', 'Product Type: CD Radio Recorder
AMPLIER:-
PMPO Power Output: 280W
RMS Power Output: 10W x2 (RMS MAX / DC)
FM: 87.5 ~ 108.0 MHz (50kHz step)
AM: 520 ~ 1630 kHz (10 kHz step)
CD SECTION:-
Playable Type: CD, CD-R/RW*1 and MP3*2', 'https://priceguru.mu/media/catalog/product/cache/c5b0e6136a6dd7f7d91d8b889ed40f35/p/r/priceguru-rx-d55gu-k.jpg','Xiaomi Deerma Handheld Vacuum Cleaner VC25', 3390, 4),
('Electronics', 'Size diagonal 	65inch
Resolution 	4K 3840 x 2160
Other Display Features 	HDR
Smart TV	Yes', 'https://priceguru.mu/media/catalog/product/cache/c5b0e6136a6dd7f7d91d8b889ed40f35/p/r/priceguru_panasonic-th-65hx750m-1.jpg','Panasonic Radio USB /Music /Digital Audio Recorder', 3990, 99)


-- ('Appliances', 'Product Type: Vacuum Cleaner Rated voltage: 21.6V Rated power: 150W', 'https://priceguru.mu/media/catalog/product/cache/c5b0e6136a6dd7f7d91d8b889ed40f35/p/r/priceguru-vc25_1.jpg','Panasonic 65" Android 4K HDR SMART LED TV', 3990, 99),

-- INSERT INTO PRODUCT VALUES(1)
-- val id: Long,
--         val title: String,
--         val year: Int,
--         val genre: String,
--         val image: String

