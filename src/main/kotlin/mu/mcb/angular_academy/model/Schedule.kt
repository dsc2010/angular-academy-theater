package mu.mcb.angular_academy.model

import java.util.*
import javax.persistence.*

@Entity
@Table(name="SCHEDULE")
data class Schedule (
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        val id: Long,

        val hall: Int,

        val dateAndTime: Date,

//        @ManyToOne
//        @JoinColumn(name = "id")
//        val theater: Theater,

//        @OneToMany(orphanRemoval = true, cascade = [CascadeType.ALL], fetch = FetchType.LAZY)
//        @JoinColumn(name = "id")
//        val movies: List<Movie>,
        ) {
//    constructor() : this(0, 1,   Date(), Theater(), mutableListOf()) {
    constructor() : this(0, 1,   Date()) {

    }

}