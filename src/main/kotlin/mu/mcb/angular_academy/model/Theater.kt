package mu.mcb.angular_academy.model

import javax.persistence.*

@Entity
@Table(name = "THEATER")
data class Theater (

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    val id: Long,
    val name: String,
    val location: String,

//    @OneToMany(mappedBy = "theater")
//    val schedules: List<Schedule>


) {
//    constructor() : this( 0 , "", "", mutableListOf()) {
    constructor() : this( 0 , "", "") {

    }
}