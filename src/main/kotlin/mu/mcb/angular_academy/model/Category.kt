package mu.mcb.angular_academy.model

import javax.persistence.*


@Entity
@Table(name="CATEGORY")
data class Category(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long,
    val name: String,
    val code: String,
    val description: String
) {

constructor(): this(0,"", "", ""){}
}