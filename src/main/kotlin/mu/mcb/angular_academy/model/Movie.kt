package mu.mcb.angular_academy.model

import javax.persistence.*

@Entity
@Table(name = "MOVIE")
data class Movie (
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long,
        val image: String,
        val genre: String,
        val title: String,
        val year: Int
        ) {
    constructor() : this(0, "", "" , "", 0) {

    }
}