package mu.mcb.angular_academy.model

import javax.persistence.*

@Entity
@Table(name="PRODUCT")
data class Product (
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long,
    val name: String,
    val description: String,
    val price: Double,
    val category: String,
    val imgUrl: String,
    val stockAvailable: Int
){
 constructor(): this(0, "", "", 0.00, "", "", 0) {}
}