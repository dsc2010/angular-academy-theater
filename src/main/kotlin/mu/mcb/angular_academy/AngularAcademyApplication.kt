package mu.mcb.angular_academy

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class AngularAcademyApplication

fun main(args: Array<String>) {
    runApplication<AngularAcademyApplication>(*args)
}
