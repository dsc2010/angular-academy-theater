package mu.mcb.angular_academy.controller

import mu.mcb.angular_academy.model.Category
import mu.mcb.angular_academy.model.Product
import mu.mcb.angular_academy.repository.CategoryRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@CrossOrigin("*")
class CategoryController {

    @Autowired
    lateinit var categoryRepository: CategoryRepository

    @GetMapping("/categories")
    fun getCategories(): ResponseEntity<List<Category>> {
        val categories = categoryRepository.findAll()
        if(categories.isEmpty()) {
            return ResponseEntity<List<Category>>(HttpStatus.NO_CONTENT)
        }
        return ResponseEntity<List<Category>>(categories, HttpStatus.OK)
    }



    @PostMapping("/categories")
    fun createCategory(@RequestBody newCategories: Category) : Category {
        return categoryRepository.save(newCategories)
    }

    @DeleteMapping("/categories/{id}")
    fun deleteCategory(@PathVariable id: Long) {
        categoryRepository.deleteById(id)
    }
}