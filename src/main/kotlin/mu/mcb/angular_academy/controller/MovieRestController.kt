package mu.mcb.angular_academy.controller

import io.swagger.v3.oas.annotations.Operation
import mu.mcb.angular_academy.model.Movie
import mu.mcb.angular_academy.repository.MovieRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class MovieRestController {

    @Autowired
    lateinit var  movieRepository: MovieRepository

    @Operation(hidden = true)
    @CrossOrigin(origins = ["*"])
    @GetMapping("/movies")
    fun fetchMovies() : ResponseEntity<List<Movie>> {
        val movies = movieRepository.findAll()
        if(movies.isEmpty()) {
            return ResponseEntity<List<Movie>>(HttpStatus.NO_CONTENT)
        }
        return ResponseEntity<List<Movie>>(movies, HttpStatus.OK)
    }

    @Operation(hidden = true)
    @CrossOrigin(origins = ["*"])
    @PostMapping("/movies")
    fun createMovie(@RequestBody newMovie: Movie) : Movie {
        return movieRepository.save(newMovie)
    }

//    @CrossOrigin(origins = ["*"])
//    @DeleteMapping("/movies/{id}")
//    fun deleteMovie(@PathVariable id: Long) :ResponseEntity<Any>  {
//        var isDelete =  movieRepository.deleteById(id)
//         if (!isDelete) {
//           return ResponseEntity<Any>(HttpStatus.NOT_FOUND)
//        }
//            return ResponseEntity<Any>(id, HttpStatus.OK)
//
//    }
}