package mu.mcb.angular_academy.controller

import io.swagger.v3.oas.annotations.Hidden
import mu.mcb.angular_academy.model.Theater
import mu.mcb.angular_academy.repository.TheaterRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@Hidden
@RestController
class TheaterRestController {

    @Autowired
    lateinit var  theaterRepository: TheaterRepository

    @CrossOrigin(origins = ["*"])
    @GetMapping("/theaters")
    fun fetchTheaters() : ResponseEntity<List<Theater>> {
        val theaters = theaterRepository.findAll()
        if(theaters.isEmpty()) {
            return ResponseEntity<List<Theater>>(HttpStatus.NO_CONTENT)
        }
        return ResponseEntity<List<Theater>>(theaters, HttpStatus.OK)
    }
}