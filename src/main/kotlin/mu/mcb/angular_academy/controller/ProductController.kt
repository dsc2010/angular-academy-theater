package mu.mcb.angular_academy.controller

import io.swagger.v3.oas.annotations.Operation
import mu.mcb.angular_academy.model.Product
import mu.mcb.angular_academy.repository.ProductRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.rest.webmvc.ResourceNotFoundException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@CrossOrigin("*")
class ProductController {

    @Autowired
    lateinit var productRepository: ProductRepository


    @Operation(summary = "Get all products or get products by category name", description = "/products will return all available products whe no query string are provided. If category name is provided in the url it will return the product based on the category name e.g. /products?categoryName=Electronics")
    @GetMapping("/products")
    fun getProducts(@RequestParam(required = false) categoryName: String?): ResponseEntity<List<Product>> {
        val product = if (categoryName != null) {
            productRepository.findByCategory(categoryName)
        } else {
            productRepository.findAll()
        }

        if (product.isEmpty()) {
            return ResponseEntity<List<Product>>(HttpStatus.NO_CONTENT)
        }
        return ResponseEntity<List<Product>>(product, HttpStatus.OK)
    }


    @Operation(summary = "Create or Update products", description = "API will create product if 'id' field is null/empty. If the 'id' field is provided, the API will update the product with the provided 'id'")
    @PostMapping("/products")
    fun createProduct(@RequestBody newProduct: Product): Product {
        return productRepository.save(newProduct)
    }


//    @Operation(summary = "Update products", description = "API will update the product for which 'id' was provided in the URL")
//    @PutMapping("/products/{id}")
//    fun updateProduct(@RequestBody newProduct: Product, @PathVariable id : Long): ResourceNotFoundException {
//
//        val productToUpdate = productRepository.findById(id)
//
//        if(productToUpdate.isPresent) {
//            return ResponseEntity(HttpStatus.OK)
//        } else {
//            return ResourceNotFoundException("Employee not found for this id :: " )
//        }
//
//
//    }


    @DeleteMapping("/products/{id}")
    fun deleteProduct(@PathVariable id: Long) {
        productRepository.deleteById(id)
    }


}