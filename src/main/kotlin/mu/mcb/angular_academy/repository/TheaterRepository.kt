package mu.mcb.angular_academy.repository

import mu.mcb.angular_academy.model.Theater
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface TheaterRepository: JpaRepository<Theater, Long> {

}