package mu.mcb.angular_academy.repository

import mu.mcb.angular_academy.model.Product
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface ProductRepository: JpaRepository<Product, Long> {

    fun findByCategory(categoryName: String): List<Product>
}