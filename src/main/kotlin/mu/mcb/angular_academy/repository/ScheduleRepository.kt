package mu.mcb.angular_academy.repository

import mu.mcb.angular_academy.model.Schedule
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface ScheduleRepository:JpaRepository<Schedule, Long> {

//    fun findByTheaterId(theaterId: Long): List<Schedule>
}